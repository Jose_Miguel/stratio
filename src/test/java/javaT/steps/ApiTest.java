package javaT.steps;
import static org.junit.Assert.*;
import javaT.cucumber.BaseStep;
import javaT.cucumber.ScenarioContext;
import javaT.pages.ApiTesting;
import cucumber.api.Scenario;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.http.ContentType;

public class ApiTest extends BaseStep {

    private ApiTesting driver;
    private Integer statusCode=0;
    private String apiResponse="";


    public ApiTest(ScenarioContext scenarioContext) {
        this.scenarioContext = scenarioContext;

        After(new String[] {"@Automation"}, (Scenario scenario) -> super.teardown(scenario));

        Given("User navigates to \"([^\"]*)\"$", (String url) -> {
            super.setup(url);
            driver = new ApiTesting(scenarioContext.getDriver());

        });


        When("User sends parameters {string}, {string} and {string}", (String title, String body, String userId) -> {
            
            RestAssured.baseURI = super.getUrl();
            
            
            String requestBody = "{\n" +
            "  \"title\": \""+title+"\",\n" +
            "  \"body\": \""+body+"\",\n" +
            "  \"userID\": \""+userId+"\"\n" +
            "}";
 
 
        Response response = null;
 
        try {
            response = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .post("/posts");
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(201, response.getStatusCode());
        System.out.println("Response :" + response.asString());
        System.out.println("El tipo de response.getStatusCode es" + ((Object)response.getStatusCode()).getClass().getSimpleName());
        System.out.println("Status Code :" + response.getStatusCode());
        apiResponse = response.asString();
        

        });

        Then("Gets {string}", (String id) -> {
        System.out.println("Status Code :" + statusCode);
        System.out.println("Does Reponse contains " + id +"?:   " + apiResponse.contains(id));
        assertTrue(apiResponse.contains(id));

        });

    }
}
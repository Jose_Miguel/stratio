@Automation
Feature: Api testing


Scenario Outline: Scenario Outline name: Testing https://jsonplaceholder.typicode.com/posts
    Given User navigates to "https://jsonplaceholder.typicode.com"
    When User sends parameters "<title>", "<body>" and "<userId>"
    Then Gets "<result>"

        Examples:
            | title | body | userId |result|
            | foo   | bar  | 1      |101|





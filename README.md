# Stratio

**Environment:**
This project was created using the next environment:
MacOSX 10.14.6
VS code

**In case of this error **
This version of ChromeDriver only supports Chrome version 75

For Mac execute 
- brew cask upgrade chromedriver
- 
For Windows execute
- choco upgrade chromedriver



**To run the framework**
1. Clone the project into a local folder
2. Open the project with the desired IDE (in this case Vscode)
3. Allow the IDE install the needed dependencies contained in the pom.xml file
4. Open the IDE terminal
5. Go to the path where the project is located
    1. /user_path/StratioAutomation
6. Execute the next command to run the test
    1. mvn test
    
Created by Jose Miguel Hernandez Neri


